﻿using System.Data.Entity;
using WebAppLibrary.Models;

namespace WebAppLibrary.Contexts
{
    public class LifeITContext:DbContext
    {
        public LifeITContext(string connectionName = "LifeIT") : base(connectionName) {

            EF_FIX.FixEfProviderServicesProblem();
            // Указывает EF, что если модель изменилась,
            // нужно воссоздать базу данных с новой структурой
            Database.SetInitializer(
                new DropCreateDatabaseIfModelChanges<LifeITContext>());
            
        }

        public DbSet<SearchService> SercheServices { get; set; }
        public DbSet<AlbumRequest> AlbumRequests { get; set; }
        public DbSet<AlbumRequestToResponse> AlbumRequestToResponses { get; set; }        
        public DbSet<iTunesResponse> iTunesResponses { get; set; }
    }
}
