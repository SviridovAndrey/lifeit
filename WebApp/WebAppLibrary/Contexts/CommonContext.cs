﻿using System.Data.Entity;

namespace WebAppLibrary.Contexts
{
    public static class CommonContext
    {
        public static DbContext GetContext(ContextEnum context, string connectionName)
        {
            DbContext c = null;
            switch(context)
            {
                case ContextEnum.LifeIT:
                    c = new LifeITContext(connectionName);
                    break;                
                default:
                    c = new LifeITContext(connectionName);
                    break;
            }

            return c;
        }
    }
}
