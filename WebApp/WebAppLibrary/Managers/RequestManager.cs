﻿using System;
using WebAppLibrary.Models;

namespace WebAppLibrary.Managers
{
    public class RequestManager
    {
        private RequestServiceManager _service;

        public RequestManager(SearchService service)
        {
            this._service = GetRequestServiceManager(service);
        }

        private RequestServiceManager GetRequestServiceManager(SearchService service)
        {
            RequestServiceManager _manager;
            if (service.Name == "iTunes")
            { _manager = new iTunesRequestServiceManager(); }
            else
            { throw new NullReferenceException(); }
            return _manager;
        }

        public string GetResponse(string query)
        { return _service.GetResponse(query); }
    }
}
