﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Reflection;
using WebAppLibrary.Models;
using WebAppLibrary.Tools;

namespace WebAppLibrary.Managers
{
    public class iTunesResponseManager
    {
        public List<iTunesResponse> GetItunesResponseListFromJson(string json)
        { return GetItunesResponseListFromJson(Transformator.GetJObject(json)); }

        public List<iTunesResponse> GetItunesResponseListFromJson(JToken jsonObject)
        {
            List<iTunesResponse> items = new List<iTunesResponse>();
            foreach (JProperty property in jsonObject.Children())
            {
                switch (property.Name)
                {
                    case "results":
                        foreach (JToken child in property.Values())
                        { items.Add(GetItunesResponseFromJson(child)); }
                        break;
                    default:
                        break;
                }
            }
            return items;
        }

        public iTunesResponse GetItunesResponseFromJson(JToken jsonObject)
        {
            iTunesResponse item = new iTunesResponse();
            PropertyInfo propInfo;
            foreach (JProperty property in jsonObject.Children())
            {
                propInfo = typeof(iTunesResponse).GetProperty(property.Name);
                if (propInfo != null)
                {
                    switch (propInfo.PropertyType.Name)
                    {
                        case "String":
                            propInfo.SetValue(item, property.Value.Value<String>());
                            break;
                        case "Int32":
                            propInfo.SetValue(item, property.Value.Value<Int32>());
                            break;
                        case "Int64":
                            propInfo.SetValue(item, property.Value.Value<Int64>());
                            break;
                        case "Single":
                            propInfo.SetValue(item, property.Value.Value<Single>());
                            break;
                        case "DateTime":
                            propInfo.SetValue(item, property.Value.Value<DateTime>());
                            break;
                        default:
                            break;
                    }
                }
            }
            return item;
        }
    }
}
