﻿using WebAppLibrary.Models;

namespace WebAppLibrary.Managers
{
    public class AlbumRequestManager
    {
        public AlbumRequest Get(string request)
        { return EFHelper.GetModelByFilter<AlbumRequest>(ar => ar.Text == request, ContextEnum.LifeIT, "LifeIT"); }

        public void Add(AlbumRequest albumRequest)
        { EFHelper.AddModel<AlbumRequest>(albumRequest, ContextEnum.LifeIT, "LifeIT"); }
    }
}
