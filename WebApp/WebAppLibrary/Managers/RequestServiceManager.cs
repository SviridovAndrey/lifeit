﻿using System;
using System.Net;
using System.Text;

namespace WebAppLibrary.Managers
{
    internal abstract class RequestServiceManager
    {
        public abstract string GetResponse(string name);
        
        protected virtual string GetResponseData(string searchStr)
        {
            string result = String.Empty;
            using (WebClient _client = new WebClient())
            {
                _client.Encoding = Encoding.UTF8;
                try
                { result = _client.DownloadString(searchStr); }
                catch (WebException we)
                { throw we; }
                catch (Exception e)
                {
                    //Log.Warning(e.ToString());
                }
            }
            return result;
        }
    }
}
