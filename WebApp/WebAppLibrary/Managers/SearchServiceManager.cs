﻿using WebAppLibrary.Models;

namespace WebAppLibrary.Managers
{
    public class SearchServiceManager
    {
        public SearchService Get(int id)
        { return EFHelper.GetModelByFilter<SearchService>(ss => ss.Id == id, ContextEnum.LifeIT, "LifeIT"); }

        public void Add(SearchService _service)
        { EFHelper.AddModel<SearchService>(_service, ContextEnum.LifeIT, "LifeIT"); }
    }
}
