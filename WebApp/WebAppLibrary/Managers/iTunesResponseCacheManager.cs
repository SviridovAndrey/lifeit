﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebAppLibrary.Models;

namespace WebAppLibrary.Managers
{
    public class iTunesResponseCacheManager
    {
        public List<iTunesResponse> GetItunesResponseListFromCache(AlbumRequest request, SearchService service)
        {
            IEnumerable<Int32> _ids =
                from artr in EFHelper.GetModelList<AlbumRequestToResponse>(o => o.AlbumRequestId == request.Id && o.SearchServiceId == service.Id, ContextEnum.LifeIT, "LifeIT")
                select artr.ResponseId;
            return EFHelper.GetModelList<iTunesResponse>(o => _ids.Contains(o.Id), ContextEnum.LifeIT, "LifeIT").ToList();
        }

        public void SaveToCache(AlbumRequest request, SearchService service, IEnumerable<iTunesResponse> cache)
        {
            IEnumerable<Int64> newCollectionIds = (from c in cache select c.collectionId).Distinct();
            IEnumerable<Int64> newTrackIds = (from c in cache select c.trackId).Distinct();
            IEnumerable<iTunesResponse> old = EFHelper.GetModelList<iTunesResponse>(r => newCollectionIds.Contains(r.collectionId) && newTrackIds.Contains(r.trackId), ContextEnum.LifeIT, "LifeIT");
            foreach (iTunesResponse temp in cache)
            {
                foreach (iTunesResponse _old in old)
                {
                    if (temp.trackId == _old.trackId && temp.collectionId == _old.collectionId)
                    {
                        temp.Id = _old.Id;
                    }
                }
            }
            EFHelper.AddModelRange<iTunesResponse>(from c in cache where c.Id == 0 select c, ContextEnum.LifeIT, "LifeIT");

            IEnumerable<Int32> _oldAlbumRequestToResponseIds =
                from a in EFHelper.GetModelList<AlbumRequestToResponse>(artr => artr.AlbumRequestId == request.Id && artr.SearchServiceId == service.Id, ContextEnum.LifeIT, "LifeIT")
                select a.ResponseId;

            EFHelper.AddModelRange<AlbumRequestToResponse>(
                from c in cache
                where !_oldAlbumRequestToResponseIds.Contains(c.Id)
                select new AlbumRequestToResponse() { AlbumRequestId = request.Id, SearchServiceId = service.Id, ResponseId = c.Id },
                ContextEnum.LifeIT, "LifeIT");
        }
    }
}
