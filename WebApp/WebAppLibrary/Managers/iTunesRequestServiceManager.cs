﻿using System.Web;

namespace WebAppLibrary.Managers
{
    internal class iTunesRequestServiceManager : RequestServiceManager
    {
        public override string GetResponse(string name)
        {
            string searchStr = string.Format(@"https://itunes.apple.com/search?term={0}&entity=musicVideo&country=RU", HttpUtility.UrlEncode(name));

            return GetResponseData(searchStr);
        }
    }
}
