﻿using System.Collections.Generic;
using WebAppLibrary.Models;

namespace WebAppLibrary.Managers
{
    public class AlbumRequestToResponseToResponseManager
    {
        public IEnumerable<AlbumRequestToResponse> GetByRequest(int requestId, int systemServiceId)
        {
            return EFHelper.GetModelList<AlbumRequestToResponse>(artr => artr.AlbumRequestId == requestId && artr.SearchServiceId == systemServiceId, ContextEnum.LifeIT, "LifeIT");
        }

        public void Add(AlbumRequestToResponse albumRequestToResponse)
        {
            EFHelper.AddModel<AlbumRequestToResponse>(albumRequestToResponse, ContextEnum.LifeIT, "LifeIT");
        }

        public void Add(IEnumerable<AlbumRequestToResponse> albumRequestToResponse)
        {
            EFHelper.AddModelRange<AlbumRequestToResponse>(albumRequestToResponse, ContextEnum.LifeIT, "LifeIT");
        }
    }
}
