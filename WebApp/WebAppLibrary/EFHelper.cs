﻿using WebAppLibrary.Contexts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace WebAppLibrary
{
    public static class EFHelper
    {
        public static IEnumerable<T> GetModelList<T>(Expression<Func<T, bool>> condition, ContextEnum baseName, string connectionName) where T : class
        {
            return CommonContext.GetContext(baseName, connectionName).Set<T>().Where(condition);
        }

        public static T GetModelByFilter<T>(Expression<Func<T, bool>> condition, ContextEnum baseName, string connectionName) where T : class
        {
            return CommonContext.GetContext(baseName, connectionName).Set<T>().Where(condition).FirstOrDefault();
        }

        public static void AddModel<T>(T model, ContextEnum baseName, string connectionName) where T : class
        {
            using (DbContext context = CommonContext.GetContext(baseName, connectionName))
            {
                context.Set<T>().Add(model);
                context.SaveChanges();
            }
        }

        public static void AddModelRange<T>(IEnumerable<T> models, ContextEnum baseName, string connectionName) where T : class
        {
            using (DbContext context = CommonContext.GetContext(baseName, connectionName))
            {
                context.Set<T>().AddRange(models);
                context.SaveChanges();
            }
        }
        public static void AddModelRange<T>(T[] models, ContextEnum baseName, string connectionName) where T : class
        {
            using (DbContext context = CommonContext.GetContext(baseName, connectionName))
            {
                context.Set<T>().AddRange(models);
                context.SaveChanges();
            }
        }

        public static void RemoveModel<T>(T model, ContextEnum baseName, string connectionName) where T : class//, IGetObject<T>
        {
            using (DbContext context = CommonContext.GetContext(baseName, connectionName))
            {
            //    T item = context.Set<T>().Where(model.GetModelCondition(model)).FirstOrDefault();
            //    context.Set<T>().Remove(item);
                context.Entry<T>(model).State = EntityState.Deleted;
                context.SaveChanges();
            }
        }

        public static void RemoveRange<T>(T[] models, ContextEnum baseName, string connectionName) where T : class
        {
            using (DbContext context = CommonContext.GetContext(baseName, connectionName))
            {
                context.Set<T>().RemoveRange(models);
                context.SaveChanges();
            }
        }

        public static void UpdateModel<T>(T model, ContextEnum baseName, string connectionName) where T : class
        {
            using (DbContext context = CommonContext.GetContext(baseName, connectionName))
            {
                context.Entry<T>(model).State = EntityState.Modified;
                context.SaveChanges();
            }
        }
    }
}
