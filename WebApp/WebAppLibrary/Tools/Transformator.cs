﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;

namespace WebAppLibrary.Tools
{
    public static class Transformator
    {
        public static JObject GetJObject(string jsonText)
        {
            JsonTextReader jsreader = new JsonTextReader(new StringReader(jsonText));
            JObject json = (JObject)new JsonSerializer().Deserialize(jsreader);
            return json;
        }

        public static JArray GetJArray(string jsonText)
        {
            JsonTextReader jsreader = new JsonTextReader(new StringReader(jsonText));
            JArray json = (JArray)new JsonSerializer().Deserialize(jsreader);
            return json;
        }
    }
}
