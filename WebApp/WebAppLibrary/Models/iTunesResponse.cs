﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAppLibrary.Models
{
    public class iTunesResponse
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }
        public String wrapperType { get; set; }
		public String kind { get; set; }        
        public Int64 artistId { get; set; }
        public Int64 collectionId { get; set; }
        public Int64 trackId { get; set; }
        public String artistName { get; set; }
        public String collectionName { get; set; }
        public String trackName { get; set; }
        public String collectionCensoredName { get; set; }
        public String trackCensoredName { get; set; }
        public String artistViewUrl { get; set; }
        public String collectionViewUrl { get; set; }
        public String trackViewUrl { get; set; }
        public String previewUrl { get; set; }
        public String artworkUrl30 { get; set; }
        public String artworkUrl60 { get; set; }
        public String artworkUrl100 { get; set; }
        public Single collectionPrice { get; set; }
        public Single trackPrice { get; set; }
        public DateTime releaseDate { get; set; }
        public String collectionExplicitness { get; set; }
        public String trackExplicitness { get; set; }
        public Int32 discCount { get; set; }
        public Int32 discNumber { get; set; }
        public Int32 trackCount { get; set; }
        public Int32 trackNumber { get; set; }
        public Int32 trackTimeMillis { get; set; }
        public String country { get; set; }
        public String currency { get; set; }
        public String primaryGenreName { get; set; }
    }
}
