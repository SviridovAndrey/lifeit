﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebAppLibrary.Models
{
    public class AlbumRequestToResponse
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }
        [Required]
        public Int32 AlbumRequestId { get; set; }
        [Required]
        public Int32 SearchServiceId { get; set; }
        [Required]
        public Int32 ResponseId { get; set; }
    }
}
