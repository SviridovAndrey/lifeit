﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApp.Areas.WebApi.Models
{
    public class Album
    {
        public string artistName { get; set; }
        public string albumName { get; set; }
        public string trackName { get; set; }
        public Int32 trackCount { get; set; }
    }
}