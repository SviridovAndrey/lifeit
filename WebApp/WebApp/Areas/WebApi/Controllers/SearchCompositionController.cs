﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using WebApp.Areas.WebApi.Models;
using WebAppLibrary.Managers;
using WebAppLibrary.Models;

namespace WebApp.Areas.WebApi.Controllers
{
    public class SearchCompositionController : ApiController
    {        
        // GET: api/SearchComposition
        public IEnumerable<Album> Get(string executor, int searchSystemId = 1)
        {
            SearchServiceManager _ssmanager = new SearchServiceManager();
            SearchService _ss = _ssmanager.Get(searchSystemId);
            if (_ss == null)
            {
                _ss = new SearchService() { Name = "iTunes" };
                _ssmanager.Add(_ss);
            }

            AlbumRequestManager _armanager = new AlbumRequestManager();
            AlbumRequest _ar = _armanager.Get(executor);
            if (_ar == null)
            {
                _ar = new AlbumRequest() { Text = executor };
                _armanager.Add(_ar);
            }

            RequestManager _requestManager = new RequestManager(_ss);
            iTunesResponseManager _manager = new iTunesResponseManager();
            iTunesResponseCacheManager _cacheManager = new iTunesResponseCacheManager();
            List<iTunesResponse> response = new List<iTunesResponse>();

            try {
                string _response = _requestManager.GetResponse(executor);
                response = _manager.GetItunesResponseListFromJson(_response);
                _cacheManager.SaveToCache(_ar, _ss, response);
            }
            catch(WebException we)
            {
                response = _cacheManager.GetItunesResponseListFromCache(_ar, _ss);
            }            
            
            IEnumerable<Album> _albums = from track in response
                                         //where track.collectionName != null
                                         group track by track.collectionName ?? track.trackName into unicAlbums
                                         orderby unicAlbums.Key
                                         select new Album() { artistName = unicAlbums.First().artistName, albumName = unicAlbums.Key, trackName = unicAlbums.First().trackName, trackCount = unicAlbums.First().trackCount };
            return _albums;
            //return new string[] { "value1", "value2" };
        }

        // GET: api/SearchComposition/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/SearchComposition
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/SearchComposition/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/SearchComposition/5
        public void Delete(int id)
        {
        }
    }
}
