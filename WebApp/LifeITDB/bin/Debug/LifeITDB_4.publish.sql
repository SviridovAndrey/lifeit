﻿/*
Скрипт развертывания для LifeIT

Этот код был создан программным средством.
Изменения, внесенные в этот файл, могут привести к неверному выполнению кода и будут потеряны
в случае его повторного формирования.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "LifeIT"
:setvar DefaultFilePrefix "LifeIT"
:setvar DefaultDataPath "c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\"
:setvar DefaultLogPath "c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\"

GO
:on error exit
GO
/*
Проверьте режим SQLCMD и отключите выполнение скрипта, если режим SQLCMD не поддерживается.
Чтобы повторно включить скрипт после включения режима SQLCMD выполните следующую инструкцию:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Для успешного выполнения этого скрипта должен быть включен режим SQLCMD.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
/*
Таблица [dbo].[__MigrationHistory] удаляется. Развертывание будет остановлено, если таблица содержит данные.
*/

IF EXISTS (select top 1 1 from [dbo].[__MigrationHistory])
    RAISERROR (N'Обнаружены строки. Обновление схемы завершено из-за возможной потери данных.', 16, 127) WITH NOWAIT

GO
/*
Таблица [dbo].[AlbumRequests] удаляется. Развертывание будет остановлено, если таблица содержит данные.
*/

IF EXISTS (select top 1 1 from [dbo].[AlbumRequests])
    RAISERROR (N'Обнаружены строки. Обновление схемы завершено из-за возможной потери данных.', 16, 127) WITH NOWAIT

GO
/*
Таблица [dbo].[AlbumRequestToResponses] удаляется. Развертывание будет остановлено, если таблица содержит данные.
*/

IF EXISTS (select top 1 1 from [dbo].[AlbumRequestToResponses])
    RAISERROR (N'Обнаружены строки. Обновление схемы завершено из-за возможной потери данных.', 16, 127) WITH NOWAIT

GO
/*
Таблица [dbo].[iTunesResponses] удаляется. Развертывание будет остановлено, если таблица содержит данные.
*/

IF EXISTS (select top 1 1 from [dbo].[iTunesResponses])
    RAISERROR (N'Обнаружены строки. Обновление схемы завершено из-за возможной потери данных.', 16, 127) WITH NOWAIT

GO
/*
Таблица [dbo].[SearchServices] удаляется. Развертывание будет остановлено, если таблица содержит данные.
*/

IF EXISTS (select top 1 1 from [dbo].[SearchServices])
    RAISERROR (N'Обнаружены строки. Обновление схемы завершено из-за возможной потери данных.', 16, 127) WITH NOWAIT

GO
PRINT N'Выполняется удаление [dbo].[__MigrationHistory]...';


GO
DROP TABLE [dbo].[__MigrationHistory];


GO
PRINT N'Выполняется удаление [dbo].[AlbumRequests]...';


GO
DROP TABLE [dbo].[AlbumRequests];


GO
PRINT N'Выполняется удаление [dbo].[AlbumRequestToResponses]...';


GO
DROP TABLE [dbo].[AlbumRequestToResponses];


GO
PRINT N'Выполняется удаление [dbo].[iTunesResponses]...';


GO
DROP TABLE [dbo].[iTunesResponses];


GO
PRINT N'Выполняется удаление [dbo].[SearchServices]...';


GO
DROP TABLE [dbo].[SearchServices];


GO
PRINT N'Обновление завершено.';


GO
